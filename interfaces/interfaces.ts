// creates a new type
interface Vehicle {
  name: string;
  year: Date;
  broken: boolean;
  summary(): string;
}

interface Reportable {
  summary(): string;
}

const oldCivic = {
  name: 'civic',
  year: new Date(),
  broken: true,
  summary(): string {
    return `Name: ${this.name}`;
  }
};

const newCivic = {
  name: 'civic',
  year: 2000,
  broken: 1
};

// long type annotations
// const printVehicle = (vehicle: { name: string; year: number; broken: boolean}): void => {
//   console.log(`Name: ${vehicle.name}`);
//   console.log(`Year: ${vehicle.year}`);
//   console.log(`Broken? ${vehicle.broken}`);
// };

//Uses interfaces
const printVehicle = (vehicle: Vehicle): void => {
  console.log(`Name: ${vehicle.name}`);
  console.log(`Year: ${vehicle.year}`);
  console.log(`Broken? ${vehicle.broken}`);
};

printVehicle(oldCivic); // correct
printVehicle(newCivic); // wrong doesnt have the correct types

const printSummary  = (item: Reportable): void => {
  console.log(item.summary()); // only accessing summary
};

const drink = {
  color: 'brown',
  carbonated: true,
  sugar: 40,
  summary(): string {
    return `My drink has ${this.sugar} grams of sugar`
  }
};

printSummary(oldCivic);
printSummary(drink);
