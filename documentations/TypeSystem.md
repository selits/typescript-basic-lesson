### Type System

`Type` - refers to the different properties + functions that a value has <br>
`value` - anything we can assign in a variable (like strings, array, function, class)<br>
`"red"` - It is a string, it is a value that has all the properties + methods that we assume that a string has

```
Properties + Methods a 'string' has in JS
charAt()
charCodeAt()
concat()
includes()
endsWith()
indexOf()
lastIndexOf()
localeCompare()
match()
```
<br>
|Type | Values That Have This Type |
|String|"hi there|""| 'Today is Monday'|
|number| .000025| -20 | 40000000|
|boolean|true|false| |
|Date| new Date()| | |
|Todo| {id:, completed: true, title: "trash"}| | |
<br><br>

### Two Types of Category
#### Primitive Types
1. number
2. boolean
3. void
4. undefined
5. string
6. symbol
7. null

#### Object Types
1. functions
2. arrays
3. classes
4. objects

### Why do we care about types?
- Types are used by the Typescript Compiler to analyze our code for errors
- Types allow other engineers to understand what values are flowing around our codebase

### Where do we use types?
- Everywhere!

