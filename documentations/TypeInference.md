### Type Inference
`Type inference` - If declaration and initialization are on the sameline,
Typescript will figure out the type of 'color' for us <br >

```
/* variable 
declartion */  /* variable intialization */
const color = 'red';
```

### Any Type
- A type just as 'string' or 'boolean' are
- Means TS has no idea what this is - can't check
for correct property references
- Avoid variables with 'any' at all cost

### Type inference for functions
- Typescript tries to figure out what type of value a function will return



