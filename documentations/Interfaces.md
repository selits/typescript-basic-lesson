### Interfaces
Interfaces + Classes = How to get really strong code reuse in TS
<br>
`interfaces` - Creates a new type, describing the property names and value types of an object
<br>

### General Strategy for Reusable Code in TS
- Create functions that accept arguments that are typed with interfaces
- Object/classes can decide to 'implement' a given interface to work with a function

