### Typed Arrays
`Typed Arrays` - Arrays where each element is some consistent type of value
<br ><br>

### Why do we care?
- TS can do type inference when extracting values from an array
- TS can prevent us from adding incompatible values to the array
- We can get help with 'map', 'forEach', 'reduce' functions
- Fexible: arrays can still contain multiple different types

### Where to use typed arrays?
- Any time we need to represent a collection of records with some arbitary sort of order

