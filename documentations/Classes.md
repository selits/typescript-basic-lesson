### Classes
- Blueprint to create an object with some fields (values) and methods (functions) to represent a 'thing'
<br>
`First` - Define a set of fields (values) and methods (functions) to represent a 'thing'
<br>
`Second` - Define a set of fields (values) and methods (functions) to represent a 'thing'
<br>

### Modifiers
`Modifiers` - are keywords that we can place on different methods and properties inside of a class
<br>
`public` = This method can be called any where, any time <br>
`private` - This method can only be called by other methods in this class <br>
`protected` - This method can be called by other methods in this class, or by other methods in child classes<br>

### Interfaces + Classes
- How we get really strong code reuse in TS


