class Vehicle {
  //color: string;

  // a constructor initiates right away
  //when you create a new class or call
  constructor(public color: string) {
    //this.color = color;
  }

  drive(): void {
    console.log('chugga chugga');
  };

  protected honk(): void {
    console.log('beep');
  }
};

const vehicle = new Vehicle('orange');
console.log(vehicle.color);

//inheritance
//child class extends Vehicle which is the parent
class Car extends Vehicle{
  constructor(public wheels: number, color: string){
    super(color);
  }
  //overwrite
  drive(): void {
    console.log('vroom');
  };

  // We use private to restrict methods that
  //other developers
  // reason we want to do that is if we might have
  //a method that very deeply manipulates a class

  private privateClass(): void {
    console.log('Private')
  };

  callPrivateClass(): void {
    this.privateClass();
    this.honk(); // child class access protected honk
  }
};

const car = new Car(5, 'red');

// const vehichle = new Vehicle(); // instance of a class
//vehichle.drive();
//vehichle.honk();

// const car = new Car();
//car.drive();
//car.honk();
//car.privateClass();// Property 'onlyInside' is private and only accessible within class 'Car'
//car.callPrivateClass(); // can call privateClass


