let apples: number = 5;
let speed: string = 'fast';
let hasName: boolean = true;

let nothingMuch: null = null;
let nothing: undefined = undefined;

// build in objects
let now: Date = new Date();

// Array
let colors: string[] = ['red', 'green', 'blue'];
let myNumbers: number[] = [1,2,3];
let truths: boolean[] = [true, true, false];

// Classes
class Car {

}

let car: Car = new Car();

// Object literal
let point: { x: number; y: number } = {
  x: 10,
  y: 20
};

// Function
// what arguements the function is going to take
// and what differnt values its going to return
// up to the void thats annotation
const logNumber: (i: number) => void = (i: number) => {
  console.log(i);
};

// When to use annotations
// 1) Function that returns the 'any' type
const json = '{"x": 10, "y": 20}';
const coordinates = JSON.parse(json);
console.log(coordinates); // {x: 10, y: 20};

const jsonBoolean = 'false';
const bool = JSON.parse(jsonBoolean);
console.log(bool); // { false };

const jsonNumber= '4';
const num = JSON.parse(jsonNumber);
console.log(num); // { 4 };

const jsonValue = '{"value": 5}';
const value = JSON.parse(jsonValue);
console.log(value); // { value: number };

const jsonName= '{"name": "alex"}';
const personName = JSON.parse(jsonName);
console.log(personName); // { name: string };

// to fix type any
const jsons = '{"x": 10, "y": 20}';
const coordinate: {x: number; y: number} = JSON.parse(json);
console.log(coordinates); // {x: 10, y: 20};
//always use annotation with functions

// 2) When we declare a variable on one line
// and intializate it later
let words = ['red', 'green', 'blue'];
let foundWord: boolean;

for (let i  = 0; i < words.length; i++){
  if (words[i] === 'green'){
    foundWord = true;
  };
};

// 3) Variable whose type cannot be inferred correctly
let numbers = [-10, -1, 12];
let numberAboveZero: boolean | number = false;

for (let i = 0; i < numbers.length; i++ ) {
  if (numbers[i] > 0 ){
    numberAboveZero =  numbers[i];
  }
}